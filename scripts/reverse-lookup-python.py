#!/bin/python
import os
import sys

print ('Argument List:', str(sys.argv))

print("Basic loop over /24 range")

for x in range(255):
    print("Check ip: " + str(sys.argv[1]) + "." + str(x))
    os.system("dig @192.168.247.149 -x " + str(sys.argv[1]) + "." + str(x) + " ANY")
