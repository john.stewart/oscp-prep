#!/bin/bash

### Usage
#  ./script.sh CIDRblock optional:FULL
#  will scan CIDRblock with -sn (no port scan) and save responsing hosts
#  then scan responding hosts further for top 1000 ports. 
#  Indlue FULL as 2nd argument to do all ports on targeted scan
###

nmap_output=/tmp/nmap-out.txt
final_output=/tmp/drill-results.txt


#need to be privlidged to access raw network sockets
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


if [ -f "$nmap_output" ]; then
	echo "ReRunning Scan..."
	rm $nmap_output
else
	echo "Running Scan..." 
fi


if [ "$2" = "FULL" ]; then
	echo " NoPort Scan -> All 65k Port Scan"
else
	echo "NoPort Scan -> Top 1000 Port Scan"
	echo "Incude FULL as 2nd arg for all port scan"
fi


echo "Against CIDR: $1" 

#begin nmap scans
nmap -n -sn $1 -oG - | awk '/Up$/{print $2}' > $nmap_output

echo "Completed the Quick Scan at:"
date

echo "Starting the Drill In Scan to ${final_output}"
if [ "$2" = "FULL" ]; then
	nmap -sV -sT -A -O -p 1-65535 -iL $nmap_output -oN $final_output
else
        nmap -sV -sT -A -O --top-ports=1000 -iL $nmap_output -oN $final_output
fi
echo "Finished::::    ${final_output}"
date
