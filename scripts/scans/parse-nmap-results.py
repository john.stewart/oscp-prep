#!/usr/bin/python
###
# Loop over an out file from a nmap scan, and parse it into a unique direcotry
#with filenames that match the ip of the host
###
import sys

uInputOne = sys.argv[1]


nmapIpLine="Nmap scan report"
fileActive = False
activeIp = ""
buffer=""
outputDir = "work/"




def writeBuffer(buffer):
    print("Generating File for: " + activeIp)
    outfile = outputDir + activeIp + ".nmap"
    outfile = outfile.replace('\'','')
    outfile = outfile.replace('\\n','')
    outfile = outfile.replace('\n','')
    text_file = open(outfile , "w")
    n = text_file.write(buffer)
    text_file.close()

with open(uInputOne) as open_file:
    for line in open_file:
       # print (line)
        if nmapIpLine in line:
            #if its 
            if(fileActive):
                writeBuffer(buffer)
                buffer = ""
            bufferSplit=line.split(' ')
            activeIp=bufferSplit[4]
            fileActive = True

        buffer+=line

#make sure to print everything to a file
writeBuffer(buffer)
