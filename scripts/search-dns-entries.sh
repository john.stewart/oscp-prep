#!/bin/bash
#run a host -l on the given subnet x.x.x/24 (all hosts agaist provided dns server

for ((c=1; c<=255; c++))
do
   host -l $1.$c $2
   echo $?
done
