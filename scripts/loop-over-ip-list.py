#!/bin/python
#This script takes the name of a txt file with contents of a new line seperated list of ip addresses
#Often this is taken from nmap -oG output and passed to  | grep Up  | cut -d ' ' -f2 > iplist.txt

import sys
import os

#Put the command you want to run against the ip address
command="enum4linux -U "

with open(sys.argv[1]) as opened_file:
    for line in opened_file:
        print(command + line)
        os.system(command + line)
